@extends('adminlte.master')

@section('content')
<div class="m-2">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">pertanyaan Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
              <div class="alert alert-success">
              {{ session('success')}}
              </div>
              @endif
              <a class="btn btn-primary mb-2" href="/pertanyaan/create">Create New pertanyaan</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th>Tanggal Dibuat</th>
                      <th>Tanggal Diperbaharui</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $pro)
                    <tr>
                    <td> {{ $key +1 }} </td>
                    <td> {{ $pro->judul }} </td>
                    <td> {{ $pro->isi }} </td>
                    <td> {{ $pro->tanggal_dibuat }} </td>
                    <td> {{ $pro->tanggal_diperbaharui }} </td>
                    <td style="display: flex;">
                        <a href="/pertanyaan/{{$pro->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/pertanyaan/{{$pro->id}}/edit" class="btn btn-default btn-sm">edit</a>
                        <form action="/pertanyaan/{{$pro->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                    </tr>
                    @empty
                    <tr>
                    <td colspan="4" align="center"> No pertanyaan</td>
                    </tr>
                    @endforelse
                    
                  </tbody>
                </table>
              </div>
            </div>
</div>
@endsection