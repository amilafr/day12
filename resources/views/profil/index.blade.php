@extends('adminlte.master')

@section('content')
<div class="m-2">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Profil Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
              <div class="alert alert-success">
              {{ session('success')}}
              </div>
              @endif
              <a class="btn btn-primary mb-2" href="/profil/create">Create New Profil</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama Lengkap</th>
                      <th>Email</th>
                      <th>Foto</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($profil as $key => $pro)
                    <tr>
                    <td> {{ $key +1 }} </td>
                    <td> {{ $pro->nama_lengkap }} </td>
                    <td> {{ $pro->email }} </td>
                    <td> {{ $pro->foto }} </td>
                    <td style="display: flex;">
                        <a href="/profil/{{$pro->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/profil/{{$pro->id}}/edit" class="btn btn-default btn-sm">edit</a>
                        <form action="/profil/{{$pro->id}}" method="post">
                        <input type="text">
                        </form>
                    </td>
                    </tr>
                    @empty
                    <tr>
                    <td colspan="4" align="center"> No Profil</td>
                    </tr>
                    @endforelse
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
</div>
@endsection