@extends('adminlte.master')

@section('content')
<div class="m-2">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Profil {{ $pro->id }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/profil/{{$pro->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">

                  <div class="form-group">
                    <label for="nama_lengkap">Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value="{{ old('nama_lengkap', $pro->nama_lengkap) }}" placeholder="Enter Nama Lengkap">
                    @error('nama_lengkap')
                    <div class="alert alert-danger">
                      {{ $message }}
                      </div>
                    @enderror
                  </div>

                  

                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ old('email', $pro->email) }}" placeholder="Enter email">
                    
                  @error('email')
                  <div class="alert alert-danger">
                  {{ $message }}
                  </div>
                  @enderror
                  </div>

                  <div class="form-group">
                  <label for="nama_lengkap">Foto</label>
                  <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="foto" name="foto" value="{{ old('foto', $pro->foto) }}">
                        <label class="custom-file-label" for="foto">Choose Foto</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                  @error('foto')
                  <div class="alert alert-danger">
                  {{ $message }}
                  </div>
                  @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
</div>
@endsection