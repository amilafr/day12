<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//latihan
Route::get('/profil/create', 'ProfilController@create');
Route::post('/profil', 'ProfilController@store');
Route::get('/profil', 'ProfilController@index');
Route::get('/profil/{id}', 'ProfilController@show');
Route::get('/profil/{id}/edit', 'ProfilController@edit');
Route::put('/profil/{id}', 'ProfilController@update');
Route::delete('/profil/{id}', 'ProfilController@destroy');

//tugas
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');

?>