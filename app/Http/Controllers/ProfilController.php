<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProfilController extends Controller
{
    public function create(){
        return view('profil.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'nama_lengkap' => 'required',
            'email' => 'required|unique:profil',
            'foto' => 'required'
        ]);

        $query = DB::table('profil')->insert([
            "nama_lengkap" =>$request["nama_lengkap"],
            "email" =>$request["email"],
            "foto" =>$request["foto"]
        ]);

        return redirect('/profil')->with('success', 'Profil Berhasil Disimpan');
    }

    public function index(){
        $profil = DB::table('profil')->get();
        //dd($profil);
        return view('profil.index', compact('profil'));
    }

    public function show($id){
        //$profil = DB::table('profil')->get();
        $pro = DB::table('profil')->where('id', $id)->first();
        return view('profil.show', compact('pro'));
    }

    public function edit($id){
        //$profil = DB::table('profil')->get();
        $pro = DB::table('profil')->where('id', $id)->first();
        return view('profil.edit', compact('pro'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama_lengkap' => 'required',
            'email' => 'required',
            'foto' => 'required'
        ]);
        
        $query = DB::table('profil')->where('id', $id)->update([
            "nama_lengkap" =>$request["nama_lengkap"],
            "email" =>$request["email"],
            "foto" =>$request["foto"]
        ]);

        return redirect('/profil')->with('success', 'Profil Berhasil Diupdate');

    }

    public function destroy($id){
        $query = DB::table('posts')->where('id', $id)->delete();
        return redirect('/profil')->with('success', 'Profil');
    }
}
