<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function form(Request $request){
        $first = $request->$first_name;
        $last = $request->$last_name;
        return view('welcome', compact('first', 'last'));
    }
}
