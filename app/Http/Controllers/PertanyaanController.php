<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required|unique:pertanyaan',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" =>$request["judul"],
            "isi" =>$request["isi"],
            "tanggal_dibuat" =>$request["tanggal_dibuat"],
            "tanggal_diperbaharui" =>$request["tanggal_diperbaharui"]
        ]);

        return redirect('/pertanyaan')->with('success', 'pertanyaan Berhasil Disimpan');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        //dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id){
        //$pertanyaan = DB::table('pertanyaan')->get();
        $pro = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('pro'));
    }

    public function edit($id){
        //$pertanyaan = DB::table('pertanyaan')->get();
        $pro = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pro'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required|unique:pertanyaan',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);
        
        $query = DB::table('pertanyaan')->where('id', $id)->update([
            "judul" =>$request["judul"],
            "isi" =>$request["isi"],
            "tanggal_dibuat" =>$request["tanggal_dibuat"],
            "tanggal_diperbaharui" =>$request["tanggal_diperbaharui"]
        ]);

        return redirect('/pertanyaan')->with('success', 'pertanyaan Berhasil Diupdate');

    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'pertanyaan Berhasil Dihapus');
    }
}
